.PHONY: get-out-interfaces
get-out-interfaces:
	@echo $(OUT_INTERFACES)

.PHONY: get-in-interfaces
get-in-interfaces:
	@echo $(IN_INTERFACES)

define get_interface
$(shell echo $(shell echo $(shell $(MAKE) -s -C ../$1 get-in-interfaces) \
	$(shell $(MAKE) -s get-out-interfaces) | \
	$(SED) 's/ /\n/g' | $(SORT) | $(UNIQ) -d) | $(CUT) -d ' ' -f1)
endef

.PHONY: source@%
source@%:
	@echo $@
	@$(MAKE) -C ../$(shell echo $@ | $(CUT) -d@ -f2) source$(shell echo $@ | $(SED) 's|^source@$(shell echo $@ | $(CUT) -d@ -f2)||g')
	@$(MAKE) -C ../$(shell echo $@ | $(CUT) -d@ -f2) in-$(call get_interface,$(shell echo $@ | $(CUT) -d@ -f2))
	@$(MAKE) out-$(call get_interface,$(shell echo $@ | $(CUT) -d@ -f2))

.PHONY: restore@%
restore@%: source@%
	@echo $@
